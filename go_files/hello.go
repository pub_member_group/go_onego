package main

import "fmt"
import "strconv"
import "strings"

func main() {
	var  i int = 999
	
	var (
		j= 100
		k=20
	)
	fmt.Println("hello go world !! " )
	fmt.Println(i)
	fmt.Println(j)
	fmt.Println(k)
	
	
	var f32 float32 = 2.2
	
	var f64 float64 = 10.3456
	
	fmt.Println("f32 is",f32,",f64 is",f64)
	
	
	
	var zi int
	
	var zf float64
	
	var zb bool
	
	var zs string
	
	fmt.Println(zi,zf,zb,zs)
	
	
	pi:=&f32
	fmt.Println(*pi)
	
	
	const fla ="kuku卡库"
	fmt.Println(fla)
	fmt.Println(&pi)
	
	const (
		one=iota +1
		two
		three
		four
	)
	fmt.Println(two,three,four)
	
	i2s:=strconv.Itoa(i)
	
	s2i,err:=strconv.Atoi(i2s)
	
	fmt.Println(i2s,s2i,err)
	
	
	s1:="hello world"
	
	//判断s1的前缀是否是H
	
	fmt.Println(strings.HasPrefix(s1,"H"))
	
	//在s1中查找字符串o
	
	fmt.Println(strings.Index(s1,"o"))
	
	//把s1全部转为大写
	
	fmt.Println(strings.ToUpper(s1))


}